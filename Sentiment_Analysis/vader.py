#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jun 25 16:23:43 2020

@author: fpannach
"""

#%% imports
from nltk.sentiment.vader import SentimentIntensityAnalyzer
import pandas as pd

#%% start analyser
analyser = SentimentIntensityAnalyzer()

#%% read stuff

df = pd.read_csv('raw_data.tsv', encoding='utf-8', sep='\t')


#%%
def sentiment_analyzer_scores(sentence):
    score = analyser.polarity_scores(sentence)
    print("{:-<40} {}".format(sentence, str(score)))
    return score

#%% analyse 
    

sent_neg = [sentiment_analyzer_scores(text)['neg'] for text in df['Text']]
sent_pos = [sentiment_analyzer_scores(text)['pos'] for text in df['Text']]
sent_neu = [sentiment_analyzer_scores(text)['neu'] for text in df['Text']]


df['Sentiment negativ'] = sent_neg 
df['Sentiment neutral'] = sent_neu 
df['Sentiment positiv'] = sent_pos 

df.to_csv('raw_data_sent.csv', sep=';', encoding='utf-8')

