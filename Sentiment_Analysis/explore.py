#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jul  2 14:37:25 2020

@author: fpannach
"""

import pandas as pd 
import matplotlib.pyplot as plt 
import seaborn as sns

df = pd.read_csv('../Fachsprache/raw_data_medical_500.csv', sep=';', header=0, encoding='utf-8')


# print(len(df['Id']))


df_filter = df.loc[df['Label'] == 'INFORMATIVE']
df_filter = df_filter.loc[df_filter['no of medical terms'] <=50]
# #df_filter = df.loc[df['Text'].str.contains('latest ')]
df_filter = df_filter.filter(items=['Label', 'no of medical terms'])
# print('Informative:', len(df_filter['Label']))

# df_plt = df_filter.groupby('Label').count()

df_filter_un = df.loc[df['Label'] == 'UNINFORMATIVE']
df_filter_un = df_filter_un.loc[df_filter_un['no of medical terms'] <=50]

df_filter_un = df_filter_un.filter(items=['Label', 'no of medical terms'])

# #df_filter_un = df_filter_un.loc[df_filter_un['Text'].str.contains('latest ')]
# df_filter_un = df_filter_un.filter(items=['Label', '#medical terms'])
# print('Uninformative:', len(df_filter_un['Label']))

print('Informative', max(df_filter['no of medical terms']), min(df['no of medical terms']))
print('Uninformative', max(df_filter_un['no of medical terms']), min(df['no of medical terms']))

df_plt = df_filter.groupby('no of medical terms').count()
df_plt_un = df_filter_un.groupby('no of medical terms').count()

#sns.catplot(x='Label', y='no of medical terms', kind="swarm", data=df_filter)

plt.plot(df_plt)
plt.plot(df_plt_un)


# plt.xlabel('contains tested')
# plt.ylabel('#Tweets')

# plt.legend(loc='best')
# plt.show()

