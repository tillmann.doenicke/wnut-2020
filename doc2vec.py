import os
from gensim.models.doc2vec import Doc2Vec, TaggedDocument
from nltk.tokenize import TweetTokenizer
from sklearn import svm

path_to_train_file = "COVID19Tweet/train.tsv"
path_to_model = "d2v.model"

def tokenize(doc):
	tokens = TweetTokenizer().tokenize(doc.lower())
	return tokens

def train_model():
	with open(path_to_train_file) as f:
		lines = f.readlines()
		lines = [line.strip().split("\t") for line in lines]
		documents = [TaggedDocument(tags=[line[2]+" | "+line[0]], words=tokenize(line[1])) for line in lines]
		model = Doc2Vec(documents, vector_size=150, dm=0, window=2, min_count=10, workers=20)
		model.train(documents, total_examples=len(documents), epochs=200)
		model.save(path_to_model)

def load_model():
	if not os.path.exists(path_to_model):
		train_model()
	model = Doc2Vec.load(path_to_model)
	return model

model = load_model()

def get_known_vector(label, id):
	return model.docvecs[label+" | "+id]

def get_unknown_vector(text):
	return model.infer_vector(tokenize(text))

if __name__ == "__main__":
	X = []
	y = []
	with open(path_to_train_file) as f:
		lines = f.readlines()
		lines = [line.strip().split("\t") for line in lines]
		X = [get_known_vector(line[2], line[0]) for line in lines]
		y = [line[2] for line in lines]
	clf = svm.SVC(kernel='linear', C = 1.0)
	clf.fit(X, y)
	c = 0
	with open("COVID19Tweet/valid.tsv") as f:
		lines = f.readlines()
		lines = [line.strip().split("\t") for line in lines]
		X = [get_unknown_vector(line[1]) for line in lines]
		y = [line[2] for line in lines]
		pred = clf.predict(X)
		for i in range(len(y)):
			if y[i] == pred[i]:
				c += 1
		print(1.0*c/len(y))