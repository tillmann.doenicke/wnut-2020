#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Jul 13 13:26:28 2020

@author: fpannach
"""
#%% imports
from nltk.corpus import stopwords
from nltk.tokenize import word_tokenize
from nltk.stem import PorterStemmer
from sklearn.feature_extraction.text import TfidfVectorizer
import json
import pickle
import os

#%% initialize
directory = '../pdf_json/'
vectorizer = TfidfVectorizer(max_features=1000)
bigram_vectorizer = TfidfVectorizer(ngram_range=(1, 2),
                                     token_pattern=r'\b\w+\b', min_df=1)
stop_words = set(stopwords.words('english')) 
ps = PorterStemmer() 
text = '' 

#%% Create whole text dataset
for filename in os.listdir(directory):
    with open(directory+filename) as json_file:
        data = json.load(json_file)
        for p in data['body_text']:
            text += p['text']


text = word_tokenize(text)
text = [x for x in text if x not in stop_words]
text = [ps.stem(x) for x in text]

#%% create tfidf model
X_unigram = vectorizer.fit_transform(text)
X_bigram = bigram_vectorizer.fit_transform(text)
print(vectorizer.get_feature_names())
print(X_unigram)

#%% save
with open('vectorizer.pk', 'wb') as fin:
     pickle.dump(vectorizer, fin)

with open('bigram_vectorizer.pk', 'wb') as fin:
     pickle.dump(vectorizer, fin)

#analyze_uni = vectorizer.build_analyzer()
#analyze_uni('This is a corona test. follow me on tiwtter')
#analyze_bi = bigram_vectorizer.build_analyzer()

