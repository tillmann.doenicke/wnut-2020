 W-NUT 2020

Task website: [http://noisy-text.github.io/2020/covid19tweet-task.html](http://noisy-text.github.io/2020/covid19tweet-task.html)

# Running Instructions

## Baseline models

The baseline models (Naive Bayes Classifier and Support Vector Machine) can be trained and evaluated as follows:

```sh
python baseline.py
python evaluator.py predictions_nbc.txt COVID19Tweet/valid.tsv
python evaluator.py predictions_svm.txt COVID19Tweet/valid.tsv
```

The models can be changed in `baseline.py`. Train and test files can be specified in lines 318-319; the prediction file can be specified as `output_file` parameter in the methods `run_nbc` and `run_svm`. The features for the SVM can be changed in the function `calc_svm_feats`.

## BERT models

The model can be trained using the jupyter notebook `COVID-Twitter-BERT/COVID19Twitter.ipynb`(on TPUs available for example at [https://colab.research.google.com](https://colab.research.google.com)). The choice of the pretrained model `digitalepidemiologylab/covid-twitter-bert` can be changed in the variable `model_name`, the number of epochs in `num_epochs`.
The data has been preprocessed using the instructions from [https://github.com/digitalepidemiologylab/covid-twitter-bert#prepare-the-data](https://github.com/digitalepidemiologylab/covid-twitter-bert#prepare-the-data).