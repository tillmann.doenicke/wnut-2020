import math
import pickle
import re
import spacy
import string
from nltk.classify import NaiveBayesClassifier
from nltk.sentiment.vader import SentimentIntensityAnalyzer
from nltk.tokenize import TweetTokenizer
from sklearn import ensemble
from sklearn import svm
from spacy.tokens import Doc


class Sample:
	"""A class for samples used by the classifiers below.
	"""
	def __init__(self):
		self.id = None     # id
		self.text = None   # raw text
		self.feats = None  # extracted features
		self.gold = None   # gold label
		self.pred = None   # predicted label


medical_data = pickle.load(open("Fachsprache/vectorizer_500.pk", "rb"))
medical_terms = medical_data.get_feature_names()

MASK_NUMBERS = True
MASK_MEDICAL = False

tknzr = TweetTokenizer()

def tokenise(text, simple=True):
	if simple:
		tokens = tknzr.tokenize(text)
		tokens = [(token if token.isupper() else token.lower()) for token in tokens]
		if MASK_NUMBERS:
			tokens = [number(token) for token in tokens]
		if MASK_MEDICAL:
			tokens = [medical(token) for token in tokens]
	else:
		text = text.replace("#", "")
		tokens = mask_tokeniser(text, lowercase)
	return tokens


def number(token):
	if re.sub("["+re.escape(string.punctuation)+"]", "", token).isnumeric() and token != "19":
		return "#NUMBER#"
	return token

def medical(token):
	if token in medical_terms:
		return "#MED#"
	return token


def tweet_tokenizer(doc):
    tokens = tknzr.tokenize(doc.text)
    return Doc(doc.vocab, tokens)

sp = spacy.load("en_core_web_lg", disable=["parser"])
sp.add_pipe(tweet_tokenizer, before="tagger")

def mask_tokeniser(text):
    doc = sp(text)
    masks = []
    for token in doc:
        num = number(token.text)
        if token.text != num:
            masks.append("#NUMBER#")
        elif token.ent_type_ in ["GPE"]:
            masks.append("#" + token.ent_type_ + "#")
        else:
            if not token.text.isupper():
	            masks.append(token.lower_)
            else:
                masks.append(token.text)
    return masks

analyser = SentimentIntensityAnalyzer()

def sentiment_analyzer_scores(sentence):
	score = analyser.polarity_scores(sentence)
	return score


def read_tweets(filename):
	samples = []
	with open(filename) as f:
		for i, line in enumerate(f):
			if i == 0:
				continue
			line = line.strip().split("\t")
			sample = Sample()
			sample.id = line[0]
			sample.text = line[1]
			sample.gold = line[2]
			samples.append(sample)
	return samples


def get_ngrams(tokens, n):
	"""Convert a list of tokens into a list of all subsequent n-tuples of tokens.
	"""
	ngrams = []
	for i in range(0, len(tokens)+1-n, 1):
		ngrams.append(tuple(tokens[i:i+n]))
	return ngrams


def build_lexicon(samples, ns, fmin):
	lexicon = dict()
	for sample in samples:
		tokens = tokenise(sample.text)
		ngrams = []
		for n in ns:
			ngrams.extend(get_ngrams(tokens, n))
		for ngram in ngrams:
			try:
				lexicon[ngram] += 1
			except KeyError:
				lexicon[ngram] = 1
	lexicon = [ngram for ngram in lexicon if lexicon[ngram] >= fmin]
	return lexicon

def calc_svm_feats(samples, ns, fmin, vec, lexicon=None, document_freqs=None):
	if lexicon is None:
		lexicon = build_lexicon(samples, ns, fmin)
	sample_ngrams = []
	for sample in samples:
		tokens = tokenise(sample.text)
		ngrams = []
		for n in ns:
			ngrams.extend(get_ngrams(tokens, n))
		sample_ngrams.append(ngrams)
	if document_freqs is None:
		document_freqs = [1.0*len([1 for ngrams in sample_ngrams if ngram in ngrams])/len(samples) for ngram in lexicon]
	for s, sample in enumerate(samples):
		ngrams = sample_ngrams[s]
		feats = [0 for i in lexicon]
		for ngram in ngrams:
			try:
				feats[lexicon.index(ngram)] += 1
			except ValueError:
				pass
		if vec == "count":
			pass
		elif vec == "binary":
			feats = [min(1, v) for v in feats]
		elif vec == "tf-idf":
			feats = [(1.0*v/max(feats)*math.log2(1.0/document_freqs[i]) if document_freqs[i] > 0 and max(feats) > 0 else 0) for i, v in enumerate(feats)]
		#scores = sentiment_analyzer_scores(sample.text)
		#feats.extend([scores["neg"], scores["neu"], scores["pos"]])
		tokens = tokenise(sample.text, simple=True)
		#feats.append(readability(tokens, sentences(sample.text)))
		#feats.append(ends_with_url(tokens))
		#feats.append(starts_with_user(tokens))
		#feats.append(opinion(tokens))
		"""# Features from Wang et al. (2015): "Making the Most of Tweet-Inherent Features for Social Spam Detection on Twitter"
		feats.append(len(tokens))
		feats.append(len(sample.text))
		uppercased = len([token for token in tokens if token.isupper()])
		feats.append(uppercased)
		feats.append(1.0*uppercased/len(tokens))
		lengths = [len(token) for token in tokens]
		feats.append(max(lengths))
		feats.append(1.0*sum(lengths)/len(tokens))
		feats.append(sample.text.count("!"))
		feats.append(sample.text.count("?"))
		feats.append(tokens.count("HTTPURL"))
		feats.append(1.0*tokens.count("HTTPURL")/len(tokens))
		feats.append(number_of_hashtags(tokens))
		feats.append(1.0*number_of_hashtags(tokens)/len(tokens))
		feats.append(tokens.count("USER"))
		feats.append(1.0*tokens.count("USER")/len(tokens))
		"""
		#feats.append(len([token for token in tokens if token in medical_terms]))
		sample.feats = feats
	return lexicon, document_freqs

def train_svm(train_samples):
	X = []
	y = []
	for sample in train_samples:
		X.append(sample.feats)
		y.append(sample.gold)
	clf = svm.SVC()
	#clf = ensemble.RandomForestClassifier()
	clf.fit(X, y)
	return clf

def test_svm(clf, test_samples, output_path=None):
	c = 0
	for sample in test_samples:
		sample.pred = clf.predict([sample.feats])[0]
		if sample.gold == sample.pred:
			c += 1
	if output_path is not None:
		open(output_path, "w+").write("\n".join([sample.pred for sample in test_samples]))
	return c/len(test_samples)

def run_svm(train_samples, test_samples, ns=[1], fmin=1, vec="count", output_path=None):
	lexicon, document_freqs = calc_svm_feats(train_samples, ns, fmin, vec)
	clf = train_svm(train_samples)
	calc_svm_feats(test_samples, ns, fmin, vec, lexicon, document_freqs)
	acc = test_svm(clf, test_samples, output_path)
	print(acc)


def calc_nbc_feats(samples, ns):
	for sample in samples:
		tokens = tokenise(sample.text)
		feats = []
		for n in ns:
			feats.extend(get_ngrams(tokens, n))
		feats = dict([(feat, True) for feat in feats])
		sample.feats = feats

def train_nbc(train_samples):
	X = []
	for sample in train_samples:
		X.append((sample.feats, sample.gold))
	nbc = NaiveBayesClassifier.train(X)
	return nbc

def test_nbc(clf, test_samples, output_path=None):
	c = 0
	for sample in test_samples:
		sample.pred = clf.classify(sample.feats)
		if sample.gold == sample.pred:
			c += 1
	if output_path is not None:
		open(output_path, "w+").write("\n".join([sample.pred for sample in test_samples]))
	return c/len(test_samples)

def run_nbc(train_samples, test_samples, ns=[1], output_path=None):
	calc_nbc_feats(train_samples, ns)
	clf = train_nbc(train_samples)
	print(clf.most_informative_features(10))
	calc_nbc_feats(test_samples, ns)
	acc = test_nbc(clf, test_samples, output_path)
	print(acc)
	

from somajo import SoMaJo
smj_tokenizer = SoMaJo(language="en_PTB")
def sentences(text):
	sents = smj_tokenizer.tokenize_text([text])
	return len(list(sents))


def syllables(token):
	token = token.lower()
	e = 0
	if token.endswith("e"):
		token = token[:-1]
		e = 1
	return max(len(re.findall("[aeiouy]+", token)), e)


def readability(tokens, num_of_sents=1):
	words = []
	comp = 0
	for token in tokens:
		if token in ["USER", "HTTPURL", "#NUMBER#"]:
			words.append(token)
			continue
		token = re.sub("["+re.escape(string.punctuation)+"]", "", token)
		if token != "":
			words.append(token)
			token = re.sub("(es|ed|ing|ly|er|est)$", "", token)
			if syllables(token) > 2:
				comp += 1
	# gunning fox index:
	return 0.4*(len(words)/num_of_sents+100.0*comp/len(words))


def number_of_hashtags(tokens):
	return len([token for token in tokens if token.startswith("#") and not token.endswith("#")])


def ends_with_url(tokens):
	if tokens[-1] == "HTTPURL":
		return 1
	return 0


def starts_with_user(tokens):
	if tokens[0] == "USER":
		return 1
	return 0


def opinion(tokens):
	opinion_words = [
		("I", "think"), 
		("I", "believe"), ("my", "belief"), 
		("I", "feel"), 
		("my", "opinion"), ("my", "mind"), 
		("my", "favorite"), ("my", "favourite"), 
		("my", "point", "of", "view"),
		("I", "know"), 
		("I", "convinced"), ("I", "confident"), 
		("speaking", "for", "myself")
	]
	for op in opinion_words:
		k = 0
		for token in tokens:
			if op[k].lower() == token.lower():
				k += 1
				if k == len(op):
					return 1
	return 0


if __name__ == "__main__":
	train_samples = read_tweets("COVID19Tweet/train.tsv")
	test_samples = read_tweets("COVID19Tweet/valid.tsv")

	"""
	                                                   # with #NUMBER#  w/o #NUMBER#
	run_nbc(train_samples, test_samples, ns=[1])       # 0.773          0.774
	run_nbc(train_samples, test_samples, ns=[1, 2])    # 0.804          0.805
	run_nbc(train_samples, test_samples, ns=[1, 2, 3]) # 0.792          0.789
	run_nbc(train_samples, test_samples, ns=[2])       # 0.759          0.765
	run_nbc(train_samples, test_samples, ns=[2, 3])    # 0.758          0.758
	run_nbc(train_samples, test_samples, ns=[3])       # 0.726          0.712
	run_nbc(train_samples, test_samples, ns=[3, 1])    # 0.802          0.796
	"""
	
	"""
	                                                                           # with #NUMBER#  w/o #NUMBER#
	run_svm(train_samples, test_samples, ns=[1], fmin=100, vec="count")        # 0.796          0.785
	run_svm(train_samples, test_samples, ns=[1, 2], fmin=100, vec="count")     # 0.801          0.792
	run_svm(train_samples, test_samples, ns=[1, 2, 3], fmin=100, vec="count")  # 0.800          0.789
	run_svm(train_samples, test_samples, ns=[2], fmin=100, vec="count")        # 0.710          0.692
	run_svm(train_samples, test_samples, ns=[2, 3], fmin=100, vec="count")     # 0.710          0.689
	run_svm(train_samples, test_samples, ns=[3], fmin=100, vec="count")        # 0.612          0.574
	run_svm(train_samples, test_samples, ns=[3, 1], fmin=100, vec="count")     # 0.795          0.787
	run_svm(train_samples, test_samples, ns=[1], fmin=100, vec="binary")       # 0.793          0.792
	run_svm(train_samples, test_samples, ns=[1, 2], fmin=100, vec="binary")    # 0.807          0.793
	run_svm(train_samples, test_samples, ns=[1, 2, 3], fmin=100, vec="binary") # 0.807          0.792
	run_svm(train_samples, test_samples, ns=[2], fmin=100, vec="binary")       # 0.717          0.691
	run_svm(train_samples, test_samples, ns=[2, 3], fmin=100, vec="binary")    # 0.725          0.690
	run_svm(train_samples, test_samples, ns=[3], fmin=100, vec="binary")       # 0.619          0.576
	run_svm(train_samples, test_samples, ns=[3, 1], fmin=100, vec="binary")    # 0.791          0.791
	"""

	"""
	                                                                           # with #NUMBER#  w/o #NUMBER#
	run_svm(train_samples, test_samples, ns=[1, 2], fmin=100, vec="tf-idf")    # 0.809          0.806
	"""

	"""
	                                                                           # with #NUMBER#  w/o #NUMBER#
	run_svm(train_samples, test_samples, ns=[1, 2], fmin=10, vec="count")      # 0.811          0.801
	run_svm(train_samples, test_samples, ns=[1, 2], fmin=10, vec="binary")     # 0.820          0.808
	"""
	
	# WINNERS:
	MASK_NUMBERS = False
	run_nbc(train_samples, test_samples, ns=[1, 2], output_path="predictions_nbc.txt")
	MASK_NUMBERS = True
	run_svm(train_samples, test_samples, ns=[1, 2], fmin=10, vec="binary", output_path="predictions_svm.txt")
	